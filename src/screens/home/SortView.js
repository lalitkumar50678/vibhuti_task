import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Slider from '@react-native-community/slider';
import Colors from '../../utilities/Colors';
import Strings from '../../utilities/Strings';
import commonStyle from './styles';
const sortView = (props) => {
  return (
    <View style={styles.sortRootViewStyle}>
      <View style={styles.sortHeaderStyle}>
        <Text style={styles.sortTxtStyle}>{Strings.DashBoard.SortBy}</Text>
        <Text style={styles.clearAllTxtStyle} onPress={props.onCloseBtnClick}>
          {Strings.DashBoard.close}
        </Text>
      </View>
      <View style={commonStyle.borderStyle} />
      <View style={styles.categortyListViewStyle}>
        <Text style={styles.sortTxtBoldStyle}>
          {Strings.DashBoard.releaseDate}
        </Text>
        <View style={{width: 1, height: 35, backgroundColor: Colors.gray}} />
        <Text style={styles.sortTxtBoldStyle}>{Strings.DashBoard.rating}</Text>
      </View>
      <View style={commonStyle.borderStyle} />
      <View style={{flexDirection: 'row', paddingHorizontal: 10}}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text
            style={styles.highToLowTxt}
            onPress={() => props.sortByReleaseDate(false)}>
            {Strings.DashBoard.NewToOld}
          </Text>
          <Text
            style={styles.highToLowTxt}
            onPress={() => props.sortByReleaseDate(true)}>
            {Strings.DashBoard.oldToNew}
          </Text>
        </View>
        <View style={{height: 30, width: 1, backgroundColor: Colors.gray}} />
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text
            style={styles.highToLowTxt}
            onPress={() => props.sortByRating(true)}>
            {Strings.DashBoard.HighToLow}
          </Text>
          <Text
            style={styles.highToLowTxt}
            onPress={() => props.sortByRating(false)}>
            {Strings.DashBoard.LowtoHigh}
          </Text>
        </View>
      </View>
    </View>
  );
};
export default sortView;

const styles = StyleSheet.create({
  sortTxtStyle: {fontFamily: 'Inter-Regular', fontSize: 14},
  sortTxtBoldStyle: {
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    alignItems: 'center',
    flex: 1,
    textAlign: 'center',
  },
  thinTxtStyle: {fontFamily: 'Inter-Thin'},
  highToLowTxt: {
    fontFamily: 'Inter-Regular',
    fontSize: 12,
    height: 25,
  },
  clearAllTxtStyle: {
    fontFamily: 'Inter-Regular',
    fontSize: 14,
    color: Colors.red,
  },
  sortRootViewStyle: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: Colors.white,
    borderRadius: 5,
    width: '100%',
    //height: 100,
  },
  sortHeaderStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    height: 40,
    alignItems: 'center',
  },
  categortyListViewStyle: {
    paddingHorizontal: 5,
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
  },
});
