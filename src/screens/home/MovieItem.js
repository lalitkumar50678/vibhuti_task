import React, {useMemo} from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import {Rating} from 'react-native-ratings';
import LinearGradient from 'react-native-linear-gradient';
import TextView from '../../utilities/TextView';
import styles from './styles';
import Strings from '../../utilities/Strings';

const MovieItem = (props) => {
  return (
    <>
      <View style={styles.movieItemRootStyle}>
        <Image style={styles.movieItemStyle} source={{uri: props.movieUrl}} />
        <View style={styles.movieRootTxtStyle}>
          <View style={styles.movieItemTxtStyle}>
            <Text style={styles.movieNameTxtStyle}>{`${props.movieName}`}</Text>
            <Rating
              //showRating
              style={styles.ratingStyle}
              startingValue={props.movieRating}
              ratingCount={5}
              imageSize={12}
              isDisabled
            />
          </View>
          <View style={styles.flexSet}>
            <Text
              style={
                styles.movieYearTxtStyle
              }>{`Year: ${props.movieYear}`}</Text>
            <TextView
              style={styles.movieDescriptionTxtStyle}
              maxLength={50}>{`${props.movieDescription}`}</TextView>
            <Text style={styles.movieTypeTxtStyle}>{`${props.movieType}`}</Text>
            <View style={styles.viewBottomStyle}>
              <View style={styles.rowStyle}>
                <Text
                  style={
                    styles.movieRateTxtStyle
                  }>{`RS ${props.moviePrice}`}</Text>
                <TouchableOpacity
                  onPress={() => props.onClickMovieBuy(props.id)}>
                  <LinearGradient
                    colors={['#4c669f', '#3b5998', '#192f6a']}
                    style={styles.buyNowBtnStyle}>
                    <Text style={styles.buyNowTxtStyle}>
                      {Strings.DashBoard.buyNow}
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.borderStyle} />
    </>
  );
};
export default MovieItem;
