import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Slider from '@react-native-community/slider';
import Colors from '../../utilities/Colors';
import Strings from '../../utilities/Strings';
import commonStyle from './styles';
const filmCategories = [
  'Action',
  'Theatrically',
  'Comedy',
  'Horror',
  'Adult',
  'Drama',
];
const FilterView = (props) => {
  const [selectedMovie, setSelectedMovie] = useState([]);
  const [moviePrice, setmoviePrice] = useState(400);
  const sliderRef = useRef(null);

  onSelectMovie = (movieCat) => {
    if (selectedMovie.includes(movieCat)) {
      console.log(selectedMovie);
      const popArr = selectedMovie.slice(movieCat);
      console.log(popArr);
      setSelectedMovie(selectedMovie.filter((item) => item != movieCat));
    } else {
      setSelectedMovie(selectedMovie.concat(movieCat));
    }
  };

  movieCategory = ({item}) => (
    <TouchableOpacity
      style={[
        styles.movieItemStyle,
        {
          backgroundColor: selectedMovie.includes(item)
            ? Colors.red
            : Colors.lightWhite,
        },
      ]}
      onPress={() => onSelectMovie(item)}>
      <Text
        style={[
          styles.movieTxtColor,
          {
            color: selectedMovie.includes(item)
              ? Colors.white
              : Colors.blackColor,
          },
        ]}>
        {item}
      </Text>
    </TouchableOpacity>
  );

  onClickClearAll = () => {
    setSelectedMovie([]);
    setmoviePrice(0);
    sliderRef.current.setNativeProps({value: 400});
    setmoviePrice(400);
    props.onCloseBtnClick();
  };

  onCloseClick = () => {
    props.onCloseBtnClick();
  };

  onApplyClick = () => {
    props.onApplyBtnClick(selectedMovie, moviePrice);
  };

  onPriceChange = (price) => {
    console.log('onPriceChange calling... ', price);
    setmoviePrice(parseInt(price));
  };

  return (
    <View style={styles.filterRootViewStyle}>
      <View style={styles.filterHeaderStyle}>
        <Text style={styles.filterTxtStyle}>Filter</Text>
        <Text style={styles.clearAllTxtStyle} onPress={onClickClearAll}>
          {Strings.DashBoard.ClearAll}
        </Text>
      </View>
      <View style={commonStyle.borderStyle} />
      <View style={styles.categortyListViewStyle}>
        <Text style={styles.thinTxtStyle}>{Strings.DashBoard.Categories}</Text>
        <FlatList
          data={filmCategories}
          renderItem={movieCategory}
          keyExtractor={(item, index) => `${index}`}
          horizontal
          ItemSeparatorComponent={() => <View style={styles.dividerStyle} />}
          showsHorizontalScrollIndicator={false}
        />
        <Text style={styles.thinTxtStyle}>{Strings.DashBoard.priceType}</Text>
        <Slider
          ref={sliderRef}
          style={styles.sliderStyle}
          minimumValue={200}
          maximumValue={400}
          value={400}
          thumbTintColor={Colors.red}
          onValueChange={onPriceChange}
          minimumTrackTintColor={Colors.red}
          maximumTrackTintColor={Colors.red}
        />
        <View style={styles.silderViewStlye}>
          <Text style={styles.thin12FontStyle}>200</Text>
          <Text style={styles.thin12FontStyle}>{moviePrice}</Text>
        </View>
      </View>

      <View style={{height: 40, flexDirection: 'row'}}>
        <TouchableOpacity
          style={commonStyle.showFilterViewStyle}
          onPress={onCloseClick}>
          <Text>{Strings.DashBoard.close}</Text>
        </TouchableOpacity>
        <View style={{width: 1, height: 40, backgroundColor: Colors.gray}} />

        <TouchableOpacity
          style={commonStyle.showFilterViewStyle}
          onPress={onApplyClick}>
          <Text>{Strings.DashBoard.apply}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default FilterView;

const styles = StyleSheet.create({
  filterTxtStyle: {fontFamily: 'Inter-Regular', fontSize: 14},
  thin12FontStyle: {fontSize: 12, fontFamily: 'Inter-Thin'},
  thinTxtStyle: {fontFamily: 'Inter-Thin'},
  clearAllTxtStyle: {
    fontFamily: 'Inter-Regular',
    fontSize: 14,
    color: Colors.red,
  },
  movieItemStyle: {
    borderRadius: 5,
    backgroundColor: Colors.lightWhite,
    paddingHorizontal: 10,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  movieTxtColor: {fontFamily: 'Inter-Regular', fontSize: 14},
  filterRootViewStyle: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: Colors.white,
    borderRadius: 5,
  },
  filterHeaderStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
    height: 40,
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  categortyListViewStyle: {paddingHorizontal: 5},
  dividerStyle: {width: 5},
  sliderStyle: {width: 300, height: 20},
  silderViewStlye: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5,
  },
});
