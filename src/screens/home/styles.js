import {StyleSheet} from 'react-native';
import Colors from '../../utilities/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  headerViewStyle: {
    width: '100%',
    height: 50,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 5,
    backgroundColor: '#f2f2f2'
  },
  headerTxtStyle: {
    fontSize: 16,
    fontFamily: 'Inter-Bold',
  },
  filterStyle: {
    fontFamily: 'Inter-Thin',
    fontSize: 10,
  },
  movieItemRootStyle: {flexDirection: 'row', paddingHorizontal: 5},
  movieItemStyle: {height: 150, width: 100},
  movieItemTxtStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  movieNameTxtStyle: {fontFamily: 'Inter-Bold', fontSize: 16},
  movieTypeTxtStyle: {fontFamily: 'Inter-Regular', fontSize: 14},
  movieYearTxtStyle: {fontFamily: 'Inter-Regular', fontSize: 12},
  movieDescriptionTxtStyle: {
    fontFamily: 'Inter-Regular',
    fontSize: 12,
  },
  buyNowTxtStyle: {
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    color: Colors.white,
  },
  buyNowBtnStyle: {
    padding: 1,
    borderRadius: 5,
    width: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  movieRateTxtStyle: {
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    marginRight: 10,
    color: Colors.blue,
  },
  viewBottomStyle: {alignItems: 'flex-end', justifyContent: 'flex-end'},
  movieRootTxtStyle: {marginHorizontal: 5, flex: 1},
  ratingStyle: {marginTop: 5},
  flexSet: {flex: 1},
  seperatorStyle: {height: 1},
  borderStyle: {height: 1, backgroundColor: Colors.gray, marginVertical: 5},
  showFilterViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  filterImageStyle: {
    width: 15,
    height: 15,
  },
});
export default styles;
