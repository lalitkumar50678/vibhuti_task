import React, {useEffect, useState} from 'react';
import {
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';
import HeaderView from '../../utilities/HeaderView';
import moviesList from './/datasource';
import MovieItem from './MovieItem';
import Colors from '../../utilities/Colors';
import images from '../../utilities/images';
import Strings from '../../utilities/Strings';
import FilterView from './FilterView';
import SortView from './SortView';
import TextView from '../../utilities/TextView';
import {getHeight} from '../../utilities/dimentions';

const HomeScreen = (props) => {
  const [isShowFilter, setShowFilter] = useState(false);
  const [isShowSort, setShowSort] = useState(false);
  const [datasource, setDatasource] = useState(moviesList);

  useEffect(() => {
    console.log('Home screen is loading.....');
  }, []);

  onClickBuy = (id) => {
    const movie = datasource.filter((item) => item.id === id);
    console.log('onClickBuy --> ', movie);
    if (movie.length > 0) {
      Alert.alert('', `Are you sure to want to buy ${movie[0].movieName} ?`, [
        {
          text: Strings.DashBoard.buy,
          onPress: () => {
            props.navigation.navigate('CardScreen', {
              movie: movie[0],
            });
            filterCloseBtnClick();
          },
        },
        {
          text: Strings.DashBoard.cancel,
          onPress: () => {},
          style: 'cancel',
        },
      ]);
    }
  };

  renderItemRow = ({item}) => (
    <MovieItem
      movieUrl={item.movieImage}
      movieName={item.movieName}
      movieYear={item.movieYear}
      movieType={item.movieType}
      movieRating={item.movieRating}
      movieDescription={item.movieDescription}
      moviePrice={item.price}
      id={item.id}
      onClickMovieBuy={(id) => onClickBuy(id)}
    />
  );

  onFilterClick = () => {
    console.log('onFilterClick calling...');
    setShowFilter(true);
  };

  onSortClick = () => {
    setShowSort(true);
  };

  closePopUp = () => {
    setShowFilter(false);
    setShowSort(false);
  };

  onApplyBtnClick = (categories, price) => {
    console.log('onApplyBtnClick calling  .. ', categories, ' price ', price);
    let updateArr;
    if (categories.length > 0 && price === 400) {
      updateArr = moviesList.filter((item) =>
        categories.includes(item.movieType),
      );
    } else if (categories.length === 0 && price < 400) {
      updateArr = moviesList.filter((item) => item.price < price);
    } else if (categories.length > 0 && price < 400) {
      updateArr = moviesList.filter(
        (item) => categories.includes(item.movieType) && item.price < price,
      );
    } else {
      updateArr = moviesList;
    }
    setDatasource(updateArr);
    closePopUp();
  };

  sortingByReleaseDate = (sortType) => {
    console.log('sortingByReleaseDate calling....');
    let updateArr;

    updateArr = moviesList.sort((item1, item2) =>
      sortType
        ? item1.movieYear > item2.movieYear
        : item1.movieYear < item2.movieYear,
    );

    setDatasource(updateArr);
    closePopUp();
  };

  
  sortingByReleaseRating = (sortType) => {
    let updateArr;

    updateArr = moviesList.sort((item1, item2) =>
      sortType
        ? item1.movieRating < item2.movieRating
        : item1.movieRating > item2.movieRating,
    );

    setDatasource(updateArr);
    closePopUp();
  };

  
  filterCloseBtnClick = () => {
    setDatasource(moviesList);
    closePopUp();
  };

  return (
    <View style={styles.container}>
      <HeaderView header={Strings.DashBoard.headerTitle} />

      <FlatList
        contentContainerStyle={{marginBottom: 10}}
        data={datasource}
        extraData={datasource}
        renderItem={renderItemRow}
        ItemSeparatorComponent={() => <View style={styles.seperatorStyle} />}
        keyExtractor={(item) => `${item.id}`}
        ListEmptyComponent={() => (
          <View
            style={[
              styles.container,
              {
                alignItems: 'center',
                justifyContent: 'center',
                height: getHeight(90),
                paddingHorizontal: 5,
              },
            ]}>
            <TextView style={{fontFamily: 'Inter-Regular', fontSize: 12}}>
              {Strings.DashBoard.movieNotFound}
            </TextView>
          </View>
        )}
      />
      <View style={{height: 40, flexDirection: 'row'}}>
        <TouchableOpacity
          style={styles.showFilterViewStyle}
          onPress={onFilterClick}>
          <Image source={images.filter} style={styles.filterImageStyle} />
          <Text>{Strings.DashBoard.filter}</Text>
        </TouchableOpacity>
        <View style={{width: 1, height: 40, backgroundColor: Colors.gray}} />

        <TouchableOpacity
          style={styles.showFilterViewStyle}
          onPress={onSortClick}>
          <Image source={images.sort} style={styles.filterImageStyle} />
          <Text>{Strings.DashBoard.sort}</Text>
        </TouchableOpacity>
      </View>
      <Modal
        animationIn="slideInUp"
        onBackButtonPress={closePopUp}
        onBackdropPress={closePopUp}
        isVisible={isShowFilter}>
        <FilterView
          onCloseBtnClick={filterCloseBtnClick}
          onApplyBtnClick={onApplyBtnClick}
        />
      </Modal>
      <Modal
        animationIn="slideInUp"
        onBackButtonPress={closePopUp}
        onBackdropPress={closePopUp}
        isVisible={isShowSort}>
        <SortView
          sortByReleaseDate={(sort) => sortingByReleaseDate(sort)}
          sortByRating={(sort) => sortingByReleaseRating(sort)}
          onCloseBtnClick={closePopUp}
        />
      </Modal>
    </View>
  );
};

export default HomeScreen;
