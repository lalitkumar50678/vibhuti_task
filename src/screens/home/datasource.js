const moviesList = [
  {
    id: 115,
    movieName: 'War',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/6/6f/War_official_poster.jpg/220px-War_official_poster.jpg',
    movieDescription: 'Kabir, a secret agent, goes rogue after a mission to catch a terrorist goes awry. However, his boss sends Khalid, another agent and his student, to track him down.',
    movieYear: 2019,
    movieType: 'Action',
    movieRating: 4,
    price: 270,
  },
  {
    id: 116,
    movieName: 'Saaho',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/6/6b/Saaho_poster.jpg',
    movieDescription: 'While trying to catch a smart thief, an undercover agent and his partner realise that the case is linked to the murder of a crime lord.',
    movieYear: 2012,
    movieType: 'Action',
    movieRating: 1,
    price: 220,
  },
  {
    id: 117,
    movieName: 'Good Newwz',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/7/76/Good_Newwz_film_poster.jpg/220px-Good_Newwz_film_poster.jpg',
      movieDescription: 'Two married couples are unsuccessful in conceiving and seek medical treatment. However, they end up in a complicated situation after their doctor makes a blunder.',
    movieYear: 2015,
    movieType: 'Comedy',
    movieRating: 5,
    price: 200,
  },
  {
    id: 118,
    movieName: 'Mission Mangal',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/b/b7/Mission_Mangal.jpg',
      movieDescription:'A group of scientists at ISRO battle in their personal and professional lives and work tirelessly towards their only motive, the Mars Orbiter Mission.',
    movieYear: 2016,
    movieType: 'Theatrically',
    movieRating: 2,
    price: 260,
  },
  {
    id: 119,
    movieName: 'Housefull 4',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/3/3c/Housefull_4_poster.jpg/220px-Housefull_4_poster.jpg',
    movieDescription: 'Three brothers are set to marry three sisters. However, a peek into the distant past reveals to one of the brothers that their brides have been mixed up in their current reincarnation.',
    movieYear: 2017,
    movieType: 'Theatrically',
    movieRating: 2,
    price: 260,
  },
  {
    id: 120,
    movieName: 'Kesari',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/c/c4/Kesari_poster.jpg/220px-Kesari_poster.jpg',
    movieDescription: 'Havildar Ishar Singh, a soldier in the British Indian Army, leads 21 Sikh soldiers in a fight against 10,000 Pashtun invaders. However, what unfolds is the greatest last stand wars of all time.',
    movieYear: 2019,
    movieType: 'Theatrically',
    movieRating: 5,
    price: 200,
  },
  {
    id: 121,
    movieName: 'Sholay',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/5/52/Sholay-poster.jpg/220px-Sholay-poster.jpg',
    movieDescription: 'Jai and Veeru, two ex-convicts, are hired by Thakur Baldev Singh, a retired policeman, to help him nab Gabbar Singh, a notorious dacoit, who has spread havoc in the village of Ramgarh.',
    movieYear: 1980,
    movieType: 'Action',
    movieRating: 3,
    price: 250,
  },
  {
    id: 122,
    movieName: 'Jai Ho',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/9/96/Jai_Ho_%282013_Hindi_film%29_poster.jpg/220px-Jai_Ho_%282013_Hindi_film%29_poster.jpg',
    movieDescription: 'A former Indian military officer devises a unique plan to promote social responsibility amongst apathetic civilians. The masses embrace his initiative, but he must confront a potent political family.',
    movieYear: 2012,
    movieType: 'Comedy',
    movieRating: 2,
    price: 280,
  },
  {
    id: 123,
    movieName: 'Jism',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/e/ee/Jism_%282003_movie_poster%29.jpg',
    movieDescription: 'Jism (transl. Body) is a 2003 Indian erotic thriller film edited and directed by Amit Saxena, written by Mahesh Bhatt, produced by Pooja Bhatt and Sujit Kumar Singh under the banner Fish Eye Network ',
    movieYear: 2003,
    movieType: 'Adult',
    movieRating: 2,
    price: 280,
  },
  {
    id: 124,
    movieName: 'Ragini MMS 2',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/2/20/Ragini_MMS_2_Poster.jpg/220px-Ragini_MMS_2_Poster.jpg',
    movieDescription: 'A director attempts to shoot an erotic horror movie in a house where, unbeknown to him, a demonic spirit resides. Even before the crew starts filming, the spirit possesses the lead actresss body.',
    movieYear: 2016,
    movieType: 'Horror',
    movieRating: 4,
    price: 290,
  },
  {
    id: 125,
    movieName: 'Main Tera Hero',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/0/06/MTH_Poster.jpg/220px-MTH_Poster.jpg',
    movieDescription: 'Seenu and Sunaina fall in love with each other when he saves her from a corrupt policeman who wishes to marry her. Meanwhile, Ayesha, a girl Seenu meets on a train journey, wants to marry him.',
    movieYear: 2018,
    movieType: 'Comedy',
    movieRating: 3,
    price: 280,
  },
  {
    id: 126,
    movieName: 'Fugly',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/6/63/Fugly_Hindi_Film_Poster.jpg',
    movieDescription: 'When a policeman blackmails college friends Dev, Gaurav, Aditya and Devi after framing them for a murder he himself committed, they do everything possible to put an end to his harassment.',
    movieYear: 2015,
    movieType: 'Drama',
    movieRating: 2,
    price: 220,
  },
  {
    id: 127,
    movieName: 'Ek Villain',
    movieImage:
      'https://upload.wikimedia.org/wikipedia/en/thumb/0/0f/Ek_Villain_Poster.jpg/220px-Ek_Villain_Poster.jpg',
    movieDescription: 'Guru is a gangster whose life changes after he falls in love with Aisha and decides to mend his ways. When Aisha gets murdered by a serial killer, Guru begins to search for the culprit.',
    movieYear: 2016,
    movieType: 'Drama',
    movieRating: 3,
    price: 230,
  },
];
export default moviesList;
