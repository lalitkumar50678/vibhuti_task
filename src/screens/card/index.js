import React, {useEffect, useState} from 'react';
import {View, Alert, Text, ActivityIndicator, Keyboard} from 'react-native';
import HeaderView from '../../utilities/HeaderView';
import styles from './styles';
import Strings from '../../utilities/Strings';
import {
  CreditCardInput,
  LiteCreditCardInput,
} from 'react-native-credit-card-input';
import stripe from 'tipsi-stripe';
import Colors from '../../utilities/Colors';
import TextView from '../../utilities/TextView';
import {stripePublicKey} from '../../utilities/Constantas';

const CardView = (props) => {
  const [loading, setLoader] = useState(false);

  useEffect(() => {
    stripe.setOptions({
      publishableKey: stripePublicKey,
      androidPayMode: 'test',
    });
  }, []);

  getCardDetail = async (card) => {
    console.log('getCardDetail --> ', card);
    if (card.valid) {
      setLoader(true);
      Keyboard.dismiss();
      const mmyy = card.values.expiry.split('/');
      const cardDetails = {
        number: card.values.number,
        expMonth: parseInt(mmyy[0]),
        expYear: parseInt(mmyy[1]),
        cvc: card.values.cvc,
      };
      console.log('getCardDetail calling... ', card);
      const token = await stripe.createTokenWithCard(cardDetails);
      setLoader(false);
      console.log('token --> ', token);
      props.navigation.navigate('BookScreen', {
        movie: props.route.params.movie,
        token: token.tokenId,
      });
    } else {
      console.log('error in card detail ');
    }
  };

  return (
    <View style={styles.contain}>
      <HeaderView header={Strings.DashBoard.cardDetail} />
      <View>
        <CreditCardInput
          placeholders={{
            number: '4242 4242 4242 4242',
            expiry: 'MM/YY',
            cvc: 'CVC',
          }}
          onChange={getCardDetail}
          autoFocus
        />
      </View>
      {loading && (
        <View style={styles.loaderStyle}>
          <View style={styles.loaderCenterView}>
            <ActivityIndicator size="large" color={Colors.blue} />
            <TextView style={styles.basicTxtStyle}>
              {Strings.card.processingPayment}
            </TextView>
          </View>
        </View>
      )}
    </View>
  );
};
export default CardView;
