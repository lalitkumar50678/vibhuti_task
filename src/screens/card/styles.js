import {StyleSheet} from 'react-native';
import Colors from '../../utilities/Colors';

const styles = StyleSheet.create({
  contain: {
    flex: 1,
  },
  errorTxt: {
    fontFamily: 'Inter-Regular',
    fontSize: 15,
    color: Colors.red,
  },
  errorViewStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  loaderStyle: {
    marginTop: 50,
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    backgroundColor: Colors.blackColor,
    opacity: 0.8,
    justifyContent: 'center',
  },
  basicTxtStyle: {
    marginTop: 10,
    textAlign: 'center',
    fontFamily: 'Inter-Regular',
    fontSize: 10,
    color: Colors.blackColor,
  },
  loaderCenterView: {
    height: 80,
    width: '70%',
    backgroundColor: Colors.white,
    borderRadius: 5,
    elevation: 1,
    alignItems: 'center',
  },
});

export default styles;
