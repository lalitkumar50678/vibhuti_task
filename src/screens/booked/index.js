import React, {useEffect} from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import HeaderView from '../../utilities/HeaderView';
import Strings from '../../utilities/Strings';

import styles from './styles';
import Colors from '../../utilities/Colors';

const BookScreen = (props) => {
  const movie = props.route.params.movie;
  const stripeToken = props.route.params.token;

 

  return (
    <View style={styles.contain}>
      <HeaderView header={Strings.book.movieBooked} />
      <View style={styles.rootViewStyle}>
        <Image style={styles.movieItemStyle} source={{uri: movie.movieImage}} />
        <View style={styles.movieViewStyle}>
          <Text>
            <Text style={styles.greenTxtStyle}>{`Your movie`}</Text>
            <Text
              style={[
                styles.boldFont,
                {color: Colors.flagThirdColor},
              ]}>{` ${movie.movieName} `}</Text>
            <Text
              style={
                styles.greenTxtStyle
              }>{`has been booked successfully.`}</Text>
          </Text>
          <Text style={styles.boldFont}>{Strings.book.movieDescription}</Text>
          <Text style={styles.basicFontStyle}>
            {movie.movieDescription.length < 150
              ? movie.movieDescription
              : `${movie.movieDescription.substring(0, 150)}...`}
          </Text>
        </View>
      </View>
      <View style={styles.divider} />
      <View style={styles.maginHoriStyle}>
        <Text style={styles.boldFont}>{Strings.book.time}</Text>
        <Text>
          <Text style={styles.boldFont}>{`${Strings.book.date} :`}</Text>
          <Text style={styles.basicFontStyle}>{`${moment(new Date()).format(
            'DD-MMMM-yyyy',
          )}`}</Text>
        </Text>
        <Text>
          <Text style={styles.boldFont}>{`${Strings.book.time} :`}</Text>
          <Text style={styles.basicFontStyle}>{`04:00 PM`}</Text>
        </Text>
      </View>
      <View style={styles.divider} />
      <View style={styles.maginHoriStyle}>
        <Text style={styles.boldFont}>{Strings.book.cost}</Text>
        <Text>
          <Text style={styles.boldFont}>{`${Strings.book.totalPrice} :`}</Text>
          <Text style={styles.basicFontStyle}>{`${movie.price}/-`}</Text>
        </Text>
      </View>
      <View style={styles.divider} />
      <View style={styles.bottomViewStyle}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.popToTop();
          }}>
          <LinearGradient
            colors={['#4c669f', '#3b5998', '#192f6a']}
            style={styles.buyNowBtnStyle}>
            <Text style={styles.buyNowTxtStyle}>{Strings.book.continue}</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BookScreen;
