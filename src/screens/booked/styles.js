import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from '../../utilities/Colors';

const styles = StyleSheet.create({
  contain: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  movieItemStyle: {
    borderRadius: 5,
    backgroundColor: Colors.lightWhite,
    marginStart: 10,
    height: 150,
    width: 100,
  },
  basicFontStyle: {
    fontSize: 14,
    fontFamily: 'Inter-Regular',
    color: Colors.blackColor,
  },
  divider: {
    height: 1,
    backgroundColor: Colors.gray,
    width: '95%',
  },
  boldFont: {
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    color: Colors.blackColor,
  },
  buyNowBtnStyle: {
    padding: 7,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buyNowTxtStyle: {
    fontFamily: 'Inter-Bold',
    fontSize: 14,
    color: Colors.white,
  },
  rootViewStyle: {flexDirection: 'row', marginBottom: 5, marginTop: 5},
  movieViewStyle: {flex: 1, marginHorizontal: 5},
  greenTxtStyle: {
    fontSize: 14,
    fontFamily: 'Inter-Regular',
    color: Colors.flagThirdColor,
  },
  maginHoriStyle: {marginHorizontal: 10},
  bottomViewStyle: {position: 'absolute', bottom: 0, width: '100%'},
});

export default styles;
