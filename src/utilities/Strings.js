import React from 'react';

export default Object.freeze({
  DashBoard: {
    headerTitle: 'Movies List',
    filter: 'Filter',
    sort: 'Sort',
    SortBy: 'Sort By',
    buyNow: 'Buy Now',
    ClearAll: 'Clear All',
    Categories: 'Category Type',
    priceType: 'Price Type',
    close: 'Close',
    apply: 'Apply',
    movieNotFound:
      'No any movie found in this category. Please try with other filter type',
    releaseDate: 'By release date',
    rating: 'By Rating',
    HighToLow: 'High to low',
    LowtoHigh: 'Low to high',
    oldToNew: 'Old to new',
    NewToOld: 'New to old',
    cardDetail: 'Card Detail',
    cancel: 'Cancel',
    buy: 'Buy',
  },
  spalsh: {
    welcomeTxt: 'Welcome To Vibhuti technology',
  },
  card: {
    cardIsInvalid: 'Enter card detail is not valid',
    processingPayment: 'We are processing your payment. Please wait ...',
    wrongCard: 'Card detail is wrong. Please check again.',
  },
  book: {
    movieBooked: 'Movie Booked',
    time: 'Time',
    date: 'Date',
    instruction: 'Instruction',
    cost: 'Cost',
    totalPrice: 'Total Price',
    continue: 'Continue discovery more movies',
    movieDescription: 'Movie description',
  },
});
