import React from 'react';
import {View, TouchableOpacity, Text, Image} from 'react-native';
import styles from '../screens/home/styles';
import TextView from './TextView';
import Strings from './Strings';
import images from './images';

const HeaderView = (props) => {
  return (
    <View style={styles.headerViewStyle}>
      <TextView style={styles.headerTxtStyle}>{props.header}</TextView>
      {/* <TouchableOpacity style={{position: 'absolute', right: 5}} onPress={props.onFilterClick}>
        <Image source={images.filter} style={{height: 25, width: 25}} />
      </TouchableOpacity> */}
    </View>
  );
};
export default HeaderView;
