import React from 'react';
import HomeScreen from '../screens/home/index';
import CardScreen from '../screens/card/index';
import BookScreen from '../screens/booked/index';
import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

const RootNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="CardScreen" component={CardScreen} />
        <Stack.Screen name="BookScreen" component={BookScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
